%%%-------------------------------------------------------------------
%%% @author nfma
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 05. Jun 2015 22:12
%%%-------------------------------------------------------------------
-module(shopping_cart_app_tests).
-author("nfma").

-include_lib("eunit/include/eunit.hrl").

empty_cart_test() ->
  ?assertEqual(0, shopping_cart_app:checkout([])).

one_orange_cart_test() ->
  ?assertEqual(25, shopping_cart_app:checkout([orange])).

one_apple_cart_test() ->
  ?assertEqual(60, shopping_cart_app:checkout([apple])).

some_fruits_cart_test() ->
  ?assertEqual(145, shopping_cart_app:checkout([apple, apple, orange, apple])).

two_apples_cart_test() ->
  ?assertEqual(60, shopping_cart_app:checkout([apple, apple])).

three_apples_cart_test() ->
  ?assertEqual(120, shopping_cart_app:checkout([apple, apple, apple])).

four_apples_cart_test() ->
  ?assertEqual(120, shopping_cart_app:checkout([apple, apple, apple, apple])).

three_oranges_cart_test() ->
  ?assertEqual(50, shopping_cart_app:checkout([orange, orange, orange])).

four_oranges_cart_test() ->
  ?assertEqual(75, shopping_cart_app:checkout([orange, orange, orange, orange])).

five_oranges_cart_test() ->
  ?assertEqual(100, shopping_cart_app:checkout([orange, orange, orange, orange, orange])).

six_oranges_cart_test() ->
  ?assertEqual(100, shopping_cart_app:checkout([orange, orange, orange, orange, orange, orange])).

one_banana_cart_test() ->
  ?assertEqual(20, shopping_cart_app:checkout([banana])).

two_banana_cart_test() ->
  ?assertEqual(20, shopping_cart_app:checkout([banana, banana])).

three_banana_cart_test() ->
  ?assertEqual(40, shopping_cart_app:checkout([banana, banana, banana])).

four_banana_cart_test() ->
  ?assertEqual(40, shopping_cart_app:checkout([banana, banana, banana, banana])).

one_banana_one_apple_cart_test() ->
  ?assertEqual(60, shopping_cart_app:checkout([banana, apple])).

two_bananas_one_apple_cart_test() ->
  ?assertEqual(80, shopping_cart_app:checkout([banana, apple, banana])).

one_banana_two_apples_cart_test() ->
  ?assertEqual(120, shopping_cart_app:checkout([apple, apple, banana])).

two_banana_three_apples_cart_test() ->
  ?assertEqual(180, shopping_cart_app:checkout([apple, apple, apple, banana, banana])).

