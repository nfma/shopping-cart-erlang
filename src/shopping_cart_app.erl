-module(shopping_cart_app).

-export([checkout/1]).

checkout(Items) -> mapsum(fun item/3, frequencies(Items)).

print(ScannedProducts) -> SubTotal = mapsum(fun item/3, ScannedProducts),
  erlang:display("Total is: " ++ unicode:characters_to_list([194, 163], utf8)
  ++ integer_to_list(SubTotal div 100) ++ "." ++ integer_to_list(SubTotal rem 100) ++
  " for " ++ integer_to_list(msize(ScannedProducts)) ++ " item(s)."),
  ScannedProducts.

item(orange, Oranges, _) -> 25 * buyThreePayTwo(Oranges);
item(apple, Apples, Items)  -> 60 *
  case maps:find(banana, Items) of
    {ok, Bananas} when Bananas >= Apples -> Apples;
    {ok, Bananas} -> Bananas + buyOneGetOneFree(Apples - Bananas);
    _ -> buyOneGetOneFree(Apples)
  end;
item(banana, Bananas, Items) -> 20 *
  case maps:find(apple, Items) of
    {ok, Apples} when Bananas >= Apples -> Bananas - Apples;
    {ok, _} -> 0;
    _ -> buyOneGetOneFree(Bananas)
  end.

buyOneGetOneFree(Count) -> Count div 2 + Count rem 2.
buyThreePayTwo(Count) -> Count rem 3 + Count div 3 * 2.

%% General

frequencies(List) -> lists:foldl(fun mappend/2, maps:new(), List).

mapsum(Fun, Items) -> maps:fold(fun(K, V, Acc) -> Acc + Fun(K, V, Items) end, 0, Items).

mappend(Item, Map) ->
  case maps:find(Item, Map) of
    {ok, Count} -> print(maps:put(Item, Count + 1, Map));
    _ -> print(maps:put(Item, 1, Map))
  end.

msize(Map) -> maps:fold(fun(_, V, Acc) -> Acc + V end, 0, Map).
